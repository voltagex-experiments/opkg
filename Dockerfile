FROM debian:testing-slim as build
RUN apt update && apt install build-essential git curl cmake libreadline-dev pkgconf -y

WORKDIR /src
RUN curl -L https://s3.amazonaws.com/json-c_releases/releases/json-c-0.17-nodoc.tar.gz | tar -zxf -
WORKDIR /src/json-c-0.17/build
RUN cmake ..
RUN make -j$(nproc) && make install

WORKDIR /src
RUN curl -L https://www.lua.org/ftp/lua-5.1.5.tar.gz | tar -zxf -
WORKDIR /src/lua-5.1.5
RUN make -j$(nproc) linux && make install

WORKDIR /src
RUN git clone --depth=1 https://git.openwrt.org/project/libubox.git
WORKDIR /src/libubox/build
RUN cmake ..
RUN make -j$(nproc) && make install

WORKDIR /src/
RUN git clone --depth=1 https://git.openwrt.org/project/ubus.git
WORKDIR /src/ubus/build
RUN cmake .. && make -j$(nproc) && make install

WORKDIR /src/
RUN git clone --depth=1 https://github.com/jow-/ucode
WORKDIR /src/ucode/build
RUN cmake .. && make -j$(nproc) && make install

WORKDIR /src/
RUN git clone --depth=1 https://git.openwrt.org/project/udebug.git
WORKDIR /src/udebug/build
RUN cmake .. && make -j$(nproc) && make install || true


WORKDIR /src/
RUN git clone --depth=1 https://git.openwrt.org/project/uci.git
WORKDIR /src/uci/build
RUN cmake .. && make -j$(nproc) && make install

WORKDIR /src
RUN git clone --depth=1 https://git.openwrt.org/project/ubox.git

WORKDIR /src/ubox/build
RUN cmake ..
RUN make -j$(nproc) && make install

WORKDIR /src/
RUN git clone --depth=1 https://git.openwrt.org/project/opkg-lede.git
WORKDIR /src/opkg-lede/
COPY *.patch .
RUN git apply *.patch
WORKDIR /src/opkg-lede/build
RUN cmake .. && make -j$(nproc)

FROM scratch as gimme-binary
COPY --from=build /src/opkg-lede/build/src/opkg-cl /opkg

FROM alpine as staging
COPY --from=build /src/opkg-lede/build/src/opkg-cl /usr/bin/opkg
RUN apk add gdb strace python3 pipx
RUN touch /etc/opkg.conf
RUN mkdir -p /usr/lib/opkg/lists
#Infinite loop in opkg
#RUN mkdir -p /usr/lib/opkg/status
RUN mkdir -p /usr/lib/opkg/info

#Segfault
#RUN printf '\nPackage:\n' >> /usr/lib/opkg/status

COPY --from=build /src/opkg-lede /src/opkg-lede

ENV PATH=$PATH:/root/.local/bin
RUN pipx install gdbgui

#nah, there's no easy overflow here
#RUN python -c "print('\nPackage:\n'+('a'*50000))" > /usr/lib/opkg/status


#EXPOSE 1234
#ENTRYPOINT gdbserver localhost:1234 opkg update

#RUN gdbgui -r 
