binary:
	BUILDKIT_PROGRESS=plain docker build . -f Dockerfile --target=gimme-binary --output=.

debug:
	BUILDKIT_PROGRESS=plain docker build . -f Dockerfile -t test

gdbgui: debug
	@echo run gdbgui -r
	BUILDKIT_PROGRESS=plain docker run -p5000:5000 -it test
